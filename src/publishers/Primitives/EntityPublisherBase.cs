using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace isr.Data.LLBLGen.Publishers
{
    /// <summary> Base class for publishing entity information. This interfaces is
    /// independent of the entity type. </summary>
    /// <remarks> 
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2010-10-03, 1.0.3928.x. </para></remarks>
    public abstract class EntityPublisherBase : INotifyPropertyChanged, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Specialized default constructor for use only by derived classes. </summary>
        protected EntityPublisherBase() : base()
        {
        }

        #region " Disposable Support "

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets the is disposed. </summary>
        /// <exception cref="NotImplementedException"> Thrown when the requested operation is
        ///                                            unimplemented. </exception>
        /// <value> The is disposed. </value>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Services.MyLog and optionally releases the
        /// managed resources.
        /// </summary>
        /// <param name="disposing"> True to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.EntityInternal = null;
                    this.RemoveEntityChangedEventHandlers();
                    this.RemoveEntityCreatedEventHandlers();
                    this.RemoveEntityPropertyChangedEventHandlers();
                    this.RemoveEntitySavedEventHandlers();
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        ~EntityPublisherBase()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion
        #endregion

        #region " I NOTIFY PROPERTY CHANGED IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Removes the property changed event handlers. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( PropertyChangedEventHandler ) item;
                }
            }
        }

        #endregion

        #region " ENTITY "

        private IEntityCore _EntityInternal;

        /// <summary>   Gets or sets the entity internal. </summary>
        /// <value> The entity internal. </value>
        private IEntityCore EntityInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._EntityInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._EntityInternal != null )
                {
                    this._EntityInternal.PropertyChanged -= this.EntityPropertyChangedHandler;
                }

                this._EntityInternal = value;
                if ( this._EntityInternal != null )
                {
                    this._EntityInternal.PropertyChanged += this.EntityPropertyChangedHandler;
                }
            }
        }

        /// <summary> Gets or sets the entity core. </summary>
        /// <value> The entity core. </value>
        private IEntityCore Entity => this.EntityInternal;

        /// <summary> Sets the entity. </summary>
        /// <param name="value"> The value. </param>
        public virtual void EntitySetter( IEntityCore value )
        {
            this.EntityInternal = value;
            this.NotifyPropertyChanged( nameof( this.Entity ) );
        }

        /// <summary> Gets or sets the name of the entity. </summary>
        /// <value> The name of the entity. </value>
        public string EntityName => this.Entity is null ? $"{this.GetType().Name}.Entity" : this.Entity.LLBLGenProEntityName;

        /// <summary> Determines whether the entity needs to be fetched. 
        ///           The entity needs to be fetched if it does not exists or is new. </summary>
        /// <returns> <c>True</c> if entity data needs to be saved; otherwise, <c>False</c>. </returns>
        public bool IsFetchRequired()
        {
            return this.Entity is null || this.IsEntityNew();
        }

        /// <summary> Determines whether the entity needs to be saved. 
        ///           The entity needs to be saved if it exists and is dirty. </summary>
        /// <returns> <c>True</c> if entity data needs to be saved; otherwise, <c>False</c>. </returns>
        public virtual bool IsSaveRequired()
        {
            return this.Entity is object && this.IsEntityDirty();
        }

        /// <summary> Determines whether an existing entity must be saved for the specified elements. Same
        /// as <see cref="IsEntityDirty">dirty</see>. </summary>
        /// <returns> <c>True</c> if a new entity needs to be created or saved or the existing entity must
        /// be saved for the specified elements; otherwise, <c>False</c>. </returns>
        public bool IsUpdateRequired()
        {
            return this.Entity is object && this.Entity.IsDirty;
        }

        /// <summary> Returns <c>True</c> if the entity was instantiated. </summary>
        /// <returns> <c>True</c> if the entity exists: not nothing; Otherwise, <c>False</c>. </returns>
        public bool HasEntity()
        {
            return this.EntityInternal is object;
        }

        /// <summary> Determines if the entity is clean: exists and not new or dirty. </summary>
        /// <returns> <c>True</c> if the entity exists and is not new or dirty; Otherwise, <c>False</c>. </returns>
        public bool IsEntityClean()
        {
            return this.Entity is object && !(this.IsEntityNew() || this.IsEntityDirty());
        }

        /// <summary> Determines if the entity is dirty: exists and dirty. </summary>
        /// <returns> <c>True</c> if the entity exists and is dirty; Otherwise, <c>False</c>. </returns>
        public bool IsEntityDirty()
        {
            return this.Entity is object && this.Entity.IsDirty;
        }

        /// <summary> Determines if the entity is new: exists and new. </summary>
        /// <returns> <c>True</c> if the entity exists and is new; Otherwise, <c>False</c>. </returns>
        public bool IsEntityNew()
        {
            return this.Entity is object && this.Entity.IsNew;
        }

        /// <summary>   Determines whether the entity has valid entity data. </summary>
        /// <remarks>   David, 2022-02-09. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <returns>
        /// A <see cref="Tuple{T1, T2}"/>{<c>True</c> if has valid entity data for the specified entity;
        /// otherwise; <c>False</c>, details if failure }
        /// </returns>
        public (bool Success, string Details) IsEntityCleanInfo()
        {
            return this.Entity is null
                ? ( false, $"Entity not set;. select {this.EntityName}" )
                : this.Entity.IsNew
                    ? (false, $"New entity;. Entity {this.EntityName } must be saved before its data can be used.")
                    : this.Entity.IsDirty ? (false, $"Entity {this.EntityName } has unsaved data;. ") : (true, string.Empty);
        }

        #endregion

        #region " ENTITY NO PUBLISH "

        /// <summary> Saves the active entity. </summary>
        /// <remarks> Does not publish the <see cref="EntitySaved">entity saved</see> event. </remarks>
        /// <returns>
        /// A <see cref="Tuple{T1, T2}"/>{<c>True</c> if entity was saved and successfully refetched;
        /// otherwise; <c>False</c>, details if failure }
        /// </returns>
        public abstract (bool Success, string Details) SaveEntity();

        /// <summary> Refetches the active entity. Also fetches related entities. Override using a full
        /// fetch if the entity has a <see cref="SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPath">prefetch
        /// path</see>. </summary>
        /// <remarks> Does not publish the <see cref="EntityChanged">entity changed</see> event. </remarks>
        /// <returns>
        /// A <see cref="Tuple{T1, T2}"/>{<c>True</c> if entity was successfully refetched;
        /// otherwise; <c>False</c>, details if failure }
        /// </returns>
        public abstract (bool Success, string Details) RefetchEntity();

        /// <summary> Instantiates a new entity. </summary>
        /// <remarks> Uses the <see cref="Publishers.EntityPublisherBase.EntityChanged">entity changed event</see> to notify of the change in
        /// entity. Use without notifications when creating a new entity before saving. </remarks>
        /// <param name="notify"> <c>True</c> to notify. </param>
        public abstract void CreateEntity( bool notify );

        #endregion

        #region " ENTITY WITH PUBLISH "

        /// <summary> Gets or sets the is fetching sentinel. Indicates that the entity is being fetched.
        /// Useful when using auto fetch on primary key changes. </summary>
        /// <value> <c>True</c> if the entity is being fetched. </value>
        public bool IsFetching { get; set; }

        /// <summary> Deletes the selected entity. </summary>
        /// <returns>
        /// A <see cref="Tuple{T1, T2}"/>{<c>True</c> if entity was successfully deleted;
        /// otherwise; <c>False</c>, details if failure }
        /// </returns>
        public abstract (bool Success, string Details) Delete();

        /// <summary> Deletes the selected entity. </summary>
        /// <param name="transaction"> The transaction. </param>
        /// <returns>
        /// A <see cref="Tuple{T1, T2}"/>{<c>True</c> if entity was successfully deleted;
        /// otherwise; <c>False</c>, details if failure }
        /// </returns>
        public abstract (bool Success, string Details) Delete( TransactionBase transaction );

        /// <summary>   Fetches related entities. </summary>
        /// <remarks>   David, 2022-02-09. </remarks>
        /// <returns>
        /// A <see cref="Tuple{T1, T2}"/>{<c>True</c> if entities were successfully fetched;
        /// otherwise; <c>False</c>, details if failure }
        /// </returns>
        public abstract (bool Success, string Details) FetchRelatedEntities();

        /// <summary>   Fetches related entities. </summary>
        /// <remarks>   David, 2022-02-09. </remarks>
        /// <param name="transaction">  The transaction. </param>
        /// <returns>
        /// A <see cref="Tuple{T1, T2}"/>{<c>True</c> if entities were successfully fetched;
        /// otherwise; <c>False</c>, details if failure }
        /// </returns>
        public abstract (bool Success, string Details) FetchRelatedEntities( TransactionBase transaction );

        /// <summary>
        /// Refetches the active entity. Also fetches related entities. Override using a full fetch if
        /// the entity has a <see cref="SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPath">prefetch
        /// path</see>.
        /// </summary>
        /// <remarks>
        /// Publishes the <see cref="EntitySaved">entity saved</see> and
        /// <see cref="EntityChanged">entity changed</see> events.
        /// </remarks>
        /// <returns>
        /// A <see cref="Tuple{T1, T2}"/>{<c>True</c> if entity was save and successfully refetched;
        /// otherwise; <c>False</c>, details if failure }
        /// </returns>
        public abstract (bool Success, string Details) Refetch();

        /// <summary>   Saves the active entity. </summary>
        /// <remarks>   David, 2022-02-09. </remarks>
        /// <returns>
        /// A <see cref="Tuple{T1, T2}"/>{<c>True</c> if entity was save and successfully refetched;
        /// otherwise; <c>False</c>, details if failure }
        /// </returns>
        public abstract (bool Success, string Details) Save();

        #endregion

        #region " EVENT HANDLERS "

        #region " ENTITY CREATED "

        /// <summary> Notifies that a new entity was created. </summary>
        /// <remarks> This allows assigning the entity to respond to property changes. </remarks>
        public virtual void OnEntityCreated()
        {
            this.OnEntityCreated( EventArgs.Empty );
        }

        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnEntityCreated( EventArgs e )
        {
            this.SyncNotifyEntityCreated( e );
        }

        /// <summary> Removes the EntityCreated event handlers. </summary>
        protected void RemoveEntityCreatedEventHandlers()
        {
            this.RemoveEntityCreatedEvent( this.EntityCreated );
        }

        /// <summary>   Removes the entity created event described by value. </summary>
        /// <remarks>   David, 2022-02-09. </remarks>
        /// <param name="value">    The value. </param>
        private void RemoveEntityCreatedEvent( EventHandler<EventArgs> value )
        {
            if ( value is null )
                return;
            foreach ( Delegate d in value.GetInvocationList() )
            {
                try
                {
                    this.EntityCreated -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToString() );
                }
            }
        }

        /// <summary> Event queue for all listeners interested in EntityCreated events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
        /// Using a custom Raise method lets you iterate through the delegate list. 
        /// </remarks>
        public event EventHandler<EventArgs> EntityCreated;

        /// <summary>   Raises the entity created event. </summary>
        /// <remarks>   David, 2022-02-09. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnEntityCreated( object sender, EventArgs e )
        {
            this.EntityCreated?.Invoke( this, e );
        }

        /// <summary> Synchronously invokes the <see cref="EntityCreated">EntityCreated Event</see>. </summary>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyEntityCreated( EventArgs e )
        {
            this.OnEntityCreated( this, e );
        }

        /// <summary> Synchronously invokes the <see cref="EntityCreated">EntityCreated Event</see>. </summary>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void NotifyEntityCreated( EventArgs e )
        {
            this.OnEntityCreated( this, e );
        }

        #endregion

        #region " ENTITY CHANGED "

        /// <summary> Updates the application entity and synchronously send the event. </summary>
        /// <remarks>
        /// David 2014-01-30. Disable auto fetch while updating the entity. Changed from a-sync to sync.
        /// <para>
        /// David, 2011-05-30, 2.1.4167. fixes bug in restoring auto fetch. </para>
        /// </remarks>
        public virtual void OnEntityChanged()
        {
            _ = this.FetchRelatedEntities();
            this.OnEntityChanged( EventArgs.Empty );
        }

        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnEntityChanged( EventArgs e )
        {
            this.SyncNotifyEntityChanged( e );
        }

        /// <summary> Removes the EntityChanged event handlers. </summary>
        protected void RemoveEntityChangedEventHandlers()
        {
            this.RemoveEntityChangedEvent( this.EntityChanged );
        }

        /// <summary>   Removes the entity changed event described by value. </summary>
        /// <remarks>   David, 2022-02-09. </remarks>
        /// <param name="value">    The value. </param>
        private void RemoveEntityChangedEvent( EventHandler<EventArgs> value )
        {
            if ( value is null )
                return;
            foreach ( Delegate d in value.GetInvocationList() )
            {
                try
                {
                    this.EntityChanged -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToString() );
                }
            }
        }

        /// <summary> Event queue for all listeners interested in EntityChanged events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
        /// Using a custom Raise method lets you iterate through the delegate list. 
        /// </remarks>
        public event EventHandler<EventArgs> EntityChanged;

        private void OnEntityChanged( object sender, EventArgs e )
        {
            this.EntityChanged?.Invoke( this, e );
        }

        /// <summary>
        /// Safely and synchronously invokes the <see cref="EntityChanged">EntityChanged Event</see>.
        /// </summary>
        /// <remarks>   David, 2022-02-09. </remarks>
        /// <param name="e">    The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyEntityChanged( EventArgs e )
        {
            this.OnEntityChanged( this, e );
        }

        /// <summary>   Notifies an entity changed. </summary>
        /// <remarks>   David, 2022-02-09. </remarks>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected void NotifyEntityChanged( EventArgs e )
        {
            this.OnEntityChanged( this, e );
        }

        #endregion

        #region " ENTITY SAVED "

        /// <summary> Notifies that a new entity was Saved. </summary>
        /// <remarks> This allows assigning the entity to respond to property changes. </remarks>
        public virtual void OnEntitySaved()
        {
            this.OnEntitySaved( EventArgs.Empty );
        }

        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnEntitySaved( EventArgs e )
        {
            this.SyncNotifyEntitySaved( e );
        }

        /// <summary> Removes the EntitySaved event handlers. </summary>
        protected void RemoveEntitySavedEventHandlers()
        {
            this.RemoveEntitySavedEvent( this.EntitySaved );
        }

        /// <summary>   Removes the entity Saved event described by value. </summary>
        /// <remarks>   David, 2022-02-09. </remarks>
        /// <param name="value">    The value. </param>
        private void RemoveEntitySavedEvent( EventHandler<EventArgs> value )
        {
            if ( value is null )
                return;
            foreach ( Delegate d in value.GetInvocationList() )
            {
                try
                {
                    this.EntitySaved -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToString() );
                }
            }
        }

        /// <summary> Event queue for all listeners interested in EntitySaved events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
        /// Using a custom Raise method lets you iterate through the delegate list. 
        /// </remarks>
        public event EventHandler<EventArgs> EntitySaved;

        private void OnEntitySaved( object sender, EventArgs e )
        {
            this.EntitySaved?.Invoke( this, e );
        }

        /// <summary>
        /// Safely and synchronously invokes the <see cref="EntitySaved">EntitySaved Event</see>.
        /// </summary>
        /// <remarks>   David, 2022-02-09. </remarks>
        /// <param name="e">    The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyEntitySaved( EventArgs e )
        {
            this.OnEntitySaved(this, e);
        }
        /// <summary>   Notifies an entity saved. </summary>
        /// <remarks>   David, 2022-02-09. </remarks>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected void NotifyEntitySaved( EventArgs e )
        {
            this.OnEntitySaved( this, e );
        }


        #endregion

        #endregion

        #region " ENTITY PROPERTY CHANGED IMPLEMENTATION "

        /// <summary> Event handler. Called by  for  events. </summary>
        /// <param name="value"> The value. </param>
        /// <returns> <c>True</c> if this field is primary key field; otherwise, <c>False</c>. </returns>
        public bool IsPrimaryKeyField( string value )
        {
            return !string.IsNullOrWhiteSpace( value ) && this.EntityInternal.Fields[value] is object && this.EntityInternal.Fields[value].IsPrimaryKey;
        }

        /// <summary> Raises the property changed event. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnEntityPropertyChanged( PropertyChangedEventArgs e )
        {
            if ( e is object )
            {
                this.SyncNotifyEntityPropertyChanged( e );
            }
        }

        /// <summary> Handles a change in the entity property. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        private void EntityPropertyChangedHandler( object sender, PropertyChangedEventArgs e )
        {
            this.OnEntityPropertyChanged( e );
        }

        /// <summary> Removes the property changed event handlers. </summary>
        protected void RemoveEntityPropertyChangedEventHandlers()
        {
            this.RemoveEntityPropertyChangedEvent( this.EntityPropertyChanged );
        }

        /// <summary>   Removes the entity Saved event described by value. </summary>
        /// <remarks>   David, 2022-02-09. </remarks>
        /// <param name="value">    The value. </param>
        private void RemoveEntityPropertyChangedEvent( PropertyChangedEventHandler value )
        {
            if ( value is null )
                return;
            foreach ( Delegate d in value.GetInvocationList() )
            {
                try
                {
                    this.EntityPropertyChanged -= ( PropertyChangedEventHandler ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToString() );
                }
            }
        }

        /// <summary> Event queue for all listeners interested in Custom events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
        /// Using a custom Raise method lets you iterate through the delegate list. 
        /// </remarks>
        public event PropertyChangedEventHandler EntityPropertyChanged;

        private void OnEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            this.EntityPropertyChanged?.Invoke( this, e );
        }

        /// <summary>
        /// Synchronously notifies (invokes) a change
        /// <see cref="System.ComponentModel.ProgressChangedEventHandler">event</see> in entity property.
        /// </summary>
        /// <remarks>
        /// Includes a work around because for some reason, the binding write value does not occur. This
        /// is dangerous because it could lead to a stack overflow. It can be used if the property
        /// changed event is raised only if the property changed.
        /// </remarks>
        /// <param name="e">    Property Changed event information. </param>
        protected void SyncNotifyEntityPropertyChanged( PropertyChangedEventArgs e )
        {
            this.OnEntityPropertyChanged( this, e );
        }

        /// <summary>
        /// Synchronously notifies (invokes) a change <see cref="PropertyChangedEventHandler">event</see>
        /// in entity property.
        /// </summary>
        /// <remarks>
        /// Includes a work around because for some reason, the binding write value does not occur. This
        /// is dangerous because it could lead to a stack overflow. It can be used if the property
        /// changed event is raised only if the property changed.
        /// </remarks>
        /// <param name="name"> The name. </param>
        protected void SyncNotifyEntityPropertyChanged( string name )
        {
            if ( !string.IsNullOrWhiteSpace( name ) )
                this.SyncNotifyEntityPropertyChanged( new PropertyChangedEventArgs( name ) );
        }

        /// <summary>
        /// Synchronously notifies (invokes) a change
        /// <see cref="System.ComponentModel.ProgressChangedEventHandler">event</see> in entity property.
        /// </summary>
        /// <remarks>
        /// Includes a work around because for some reason, the binding write value does not occur. This
        /// is dangerous because it could lead to a stack overflow. It can be used if the property
        /// changed event is raised only if the property changed.
        /// </remarks>
        /// <param name="e">    Property Changed event information. </param>
        protected void NotifyEntityPropertyChanged( PropertyChangedEventArgs e )
        {
            this.OnEntityPropertyChanged( this, e );
        }

        /// <summary>
        /// Synchronously notifies (invokes) a change <see cref="PropertyChangedEventHandler">event</see>
        /// in entity property.
        /// </summary>
        /// <remarks>
        /// Includes a work around because for some reason, the binding write value does not occur. This
        /// is dangerous because it could lead to a stack overflow. It can be used if the property
        /// changed event is raised only if the property changed.
        /// </remarks>
        /// <param name="name"> The name. </param>
        protected void NotifyEntityPropertyChanged( string name )
        {
            if ( !string.IsNullOrWhiteSpace( name ) )
                this.SyncNotifyEntityPropertyChanged( new PropertyChangedEventArgs( name ) );
        }



        #region " NOTIFY PROPERTY CHANGE "

        /// <summary> Gets or sets the publishable. </summary>
        /// <value> The publishable. </value>
        public bool Publishable { get; set; }

        /// <summary> Resume publishing. </summary>
        public void ResumePublishing()
        {
            this.Publishable = true;
        }

        /// <summary> Suspend publishing. </summary>
        public void SuspendPublishing()
        {
            this.Publishable = false;
        }

        #endregion

        #endregion

    }
}

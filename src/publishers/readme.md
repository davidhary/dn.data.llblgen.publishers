# About

isr.Data.LLBLGen.Publishers is a .Net library defining LLBLGen based entity classes with publishing methods and events.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.Data.LLBLGen.Publishers is released as open source under the MIT license.
Bug reports and contributions are welcome at the [LLBLGen Publishers Repository].

[LLBLGen Publishers Repository]: https://bitbucket.org/davidhary/dn.data.llblgen.publishers


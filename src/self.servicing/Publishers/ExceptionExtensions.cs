using System;

namespace isr.Data.LLBLGen.SelfServising.ExceptionExtensions
{

    /// <summary>
    /// Exception methods for adding exception data and building a detailed exception message.
    /// </summary>
    /// <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static partial class ExceptionDataMethods
    {

        /// <summary> Adds an exception data to 'exception'. </summary>
        /// <param name="value">     The value. </param>
        /// <param name="exception"> Details of the exception. </param>
        private static bool AddExceptionData( Exception value, SD.LLBLGen.Pro.ORMSupportClasses.ORMQueryExecutionException exception )
        {
            if ( exception is object )
            {
                value.Data.Add( $"{value.Data.Count}-Query", exception.QueryExecuted );
            }

            return exception is object;
        }

        /// <summary> Adds exception data from the specified exception. </summary>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if exception was added; otherwise <c>false</c> </returns>
        public static bool AddExceptionData( this Exception exception )
        {
            return AddFrameworkExceptionData( exception ) ||
                   AddExceptionData( exception, exception as SD.LLBLGen.Pro.ORMSupportClasses.ORMQueryExecutionException );
        }

    }
}

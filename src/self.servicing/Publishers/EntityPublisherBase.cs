using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;

using isr.Data.LLBLGen.SelfServicingEntityExtensions;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace isr.Data.LLBLGen.Publishers.SelfServicing
{
    /// <summary> Base class for publishing entity information. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2010-10-03, 1.0.3928.x. </para></remarks>
    public abstract class EntityPublisherBase<TEntity> : EntityPublisherBase where TEntity : EntityBase, IEntity
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs the class. </summary>
        protected EntityPublisherBase() : base()
        {
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                          <c>False</c> to release only unmanaged resources when called from the
        ///                          runtime finalize. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.EntityInternal = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " O/R MAPPING "

        /// <summary> Builds prefetch path for the entity. </summary>
        /// <returns> The <see cref="SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPath">prefetch path</see> </returns>
        protected abstract IPrefetchPath BuildPrefetchPath();

        #endregion

        #region " ENTITY "


        private TEntity EntityInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get; [MethodImpl( MethodImplOptions.Synchronized )]
            set;
        }

        /// <summary> Gets a reference to the entity. </summary>
        /// <value> The entity. </value>
        public TEntity Entity => this.EntityInternal;

        #endregion

        #region " ENTITY NO PUBLISH "

        /// <summary> Saves the active entity without publishing events. </summary>
        /// <returns> <c>True</c> if entity was save and successfully refetched. </returns>
        public override (bool Success, string Details) SaveEntity()
        {
            string fieldNames = string.Join( ",", this.Entity.GetPrimaryKeyFieldNames().ToArray() );
            string activity = $"saving entity; '{this.Entity}' '{fieldNames}'";
            if ( this.Entity.Save() )
            {
                activity = $"fetching entity after saving;. '{this.Entity}' '{fieldNames}' from the database after saving.";
                // refetch to keep entity active without raising events.
                (bool success, string details) = this.RefetchEntity();
                // 
                return success ? ( success, details ) : (false, $"Failed {activity};. details: {details}");
            }
            else
            {
                return ( false, $"Failed {activity}" ) ;
            }
        }

        /// <summary> Sets the entity. </summary>
        /// <param name="value"> The value. </param>
        public void EntitySetter( TEntity value )
        {
            this.EntityInternal = value;
            base.EntitySetter( value );
        }

        #endregion

        #region " ENTITY WITH PUBLISH "

        /// <summary> Deletes the selected entity. </summary>
        /// <remarks> Raises the <see cref="EntityPublisherBase.EntityCreated">entity created</see> and the
        /// <see cref="EntityPublisherBase.EntityChanged">entity changed</see> events. </remarks>
        /// <param name="transaction"> The transaction. </param>
        /// <returns>
        /// A <see cref="Tuple{T1, T2}"/>{<c>True</c> if entity was successfully deleted;
        /// otherwise; <c>False</c>, details if failure }
        /// </returns>
        public override (bool Success, string Details) Delete( TransactionBase transaction )
        {
            ( bool success, string details ) = ( true, string.Empty );
            if ( !( this.Entity is null || this.Entity.IsNew ) )
            {
                if ( transaction is null )
                {
                    throw new ArgumentNullException( nameof( transaction ) );
                }

                try
                {
                    transaction.Add( this.Entity );
                    success = this.Entity.Delete();
                    transaction.Commit();
                }
                catch ( ORMQueryExecutionException ex )
                {
                    transaction.Rollback();
                    ( success, details ) = ( false, $"Exception occurred deleting entity;. '{this.Entity}'{ex.QueryExecuted}{ex}" );
                }
                finally
                {
                    if ( success )
                    {
                        this.CreateEntity( true );
                        this.OnEntityChanged();
                    }
                }
            }

            return ( success, details );
        }

        /// <summary> Saves the active entity. </summary>
        /// <returns>
        /// A <see cref="Tuple{T1, T2}"/>{<c>True</c> if entity was successfully deleted;
        /// otherwise; <c>False</c>, details if failure }
        /// </returns>
        public override (bool Success, string Details) Save()
        {
            ( bool success, string details ) = this.SaveEntity();
            if ( success )
            {
                // raise the saved and let the calling application decide if need to do entity changed and entities changed.
                this.OnEntitySaved();
            }
            return (success, details);
        }

        #endregion

    }
}

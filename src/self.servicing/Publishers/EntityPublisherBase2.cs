using SD.LLBLGen.Pro.ORMSupportClasses;

namespace isr.Data.LLBLGen.Publishers.SelfServicing
{
    /// <summary> Base class for anonymously publishing entity information for an entity that has a
    /// primary key consisting of two values. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2010-10-03, 1.0.3928.x. </para></remarks>
    public abstract class EntityAnonPublisherBase2<TEntity, TPrimaryKey1, TPrimaryKey2> : EntityPublisherBase<TEntity> where TEntity : EntityBase, IEntity
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs the class. </summary>
        protected EntityAnonPublisherBase2() : base()
        {
        }

        #endregion

        #region " ENTITY BASE METHOD - NO EVENTS "

        /// <summary> Fetches an entity or clears exiting entity. </summary>
        /// <param name="primaryKey1"> Specifies the first primary key. </param>
        /// <param name="primaryKey2"> Specifies the second primary key. </param>
        /// <returns> <c>True</c> if entity exists. Otherwise, a new entity is created. </returns>
        /// <remarks> Raises the <see cref="Publishers.EntityPublisherBase.EntityCreated">entity created</see> event. Does not raise
        /// the <see cref="Publishers.EntityPublisherBase.EntityChanged">entity changed</see> event. </remarks>
        protected abstract bool FetchEntity( TPrimaryKey1 primaryKey1, TPrimaryKey2 primaryKey2 );

        #endregion

        #region " ENTITY "

        /// <summary> Fetches a new or exiting adapter using the primary key. </summary>
        /// <param name="primaryKey1"> Specifies the first primary key. </param>
        /// <param name="primaryKey2"> Specifies the second primary key. </param>
        /// <returns> <c>True</c> if entity exists. Otherwise, a new entity is created. </returns>
        public virtual bool Fetch( TPrimaryKey1 primaryKey1, TPrimaryKey2 primaryKey2 )
        {
            _ = this.FetchEntity( primaryKey1, primaryKey2 );
            this.OnEntityChanged();
            return !this.Entity.IsNew;
        }

        /// <summary> Checks if an entity exists. </summary>
        /// <remarks> Find does not trigger the entity changed event. </remarks>
        /// <param name="primaryKey1"> Specifies the first primary key. </param>
        /// <param name="primaryKey2"> Specifies the second primary key. </param>
        /// <returns> <c>True</c> if entity exists. </returns>
        public abstract bool Exists( TPrimaryKey1 primaryKey1, TPrimaryKey2 primaryKey2 );

        #endregion

    }
}
